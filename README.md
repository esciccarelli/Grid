
Nombre: Esteban Adrián Ciccarelli<br />
Edad: 27 años<br />
Direccion: Andrade 1779<br />
Nacionalidad: argentino<br />
Teléfono: 11 31131537<br />

Una práctica realizada. Se trata de una grilla genérica que se puede utilizar y configurar a gustos del usuario. no importa el objeto que uno esté
trabajando te permite amoldarla a necesidad. <br />
NO se utilizó ningun componente ni directiva que te resuelva la lógica y funcionalidad de la grilla. ya que existen algunas como dirPagination que te resuelve el paginado o mismo componentes que te muestran
el contenido pero te limitan funcionalidades específicas.


<br />
Lo utilizado en este proyecto fueron<br />
<br />
<br />
**Directives**<br />
**Components**<br />
**Filters**<br />
**Factory/Services**<br />
**Watchs**<br />
**Controllers**<br />
**Bootstrap**<br />
<br />
<br />



Para este desarrollo los puntos a tener en cuenta son los siguientes:<br />

GRILLA:<br />

Para utilizar la grilla en el html: (list.html)<br />

<grid-table data="data" config="gridConfig"></grid-table><br />

CONFIGURACIÓN: (listController.js)<br />

$ctrl.gridConfig =<br />
		{	<br />
			pagination:{<br />
				rowsForPage: [NUMBER]<br />
			},<br />
			data:<br />
			[<br />
				{<br />
					attr: ['STRING'][ARRAY[STRING]][REQUIRED],		<br />
					colName: [STRING],<br />
					order: [BOOLEAN],<br />
					reverse: [BOOLEAN],<br />
					filter: [BOOLEAN],<br />
					render: [FUNCTION],<br />
					action: [FUNCTION],<br />
					sortFunction: [FUNCTION]<br />
				}<br />
			]<br />
		}<br />
		
pagination: (object)<br />
data: ([object, object]) cada objeto representa una columna a mostrar<br />
rowsForPage: (number) define cantidad de filas a mostrar en la grilla.<br />
attr: ('string' | ['string', 'string']) define que campo del array principal se quiere mostrar por columna<br />
colName: ('string') define el nombre de la columna que se quiere mostrar.<br />
order: (true | false) define de forma booleana si esa columna permite ordenamiento<br />
reverse: (true | false) defina la orientacion principal a utilizar en el momento de ordenar el contenido ( el componente lo seteará en asc en caso que no tenga)<br />
render: (function(){return '<html>' }) a travez de una funcion se puede mostrar en pantalla un contenido ( html )<br />
action: (function(){ }) es la accion que se le quiere poner a un elemento renderizado en pantalla ( por ejemplo funcionalidad de un botón)<br />
sortFunction: (function(a,b){ if(a.type === 'object'){ return }) permite definir por medio de una funcion una forma de ordenamiento (requiere preguntar object.type === 'object')<br />


BOTONES EN PANTALLA: (Reperesentada por medio de una función del atributo ACTION)<br />



PARA CORRER APLICACIÓN:<br />

Para levantar  se utilizo un servidor local (WAMP, XAMPP, SIMILARES).<br />
abrir localhost/delivery/#!/ o con el puerto (dependerá de como se tenga configurado)<br />
La aplicacion corre a partir de un archivo index.html donde se levantaran el contenido de las otras vistas por medio de un ng-view.<br />
Si se coloca una ruta no válida te dirige automaticamente al LIST como vista principal.<br />